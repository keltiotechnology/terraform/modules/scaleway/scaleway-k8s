# Terraform scaleway k8s module

Deploy a kubernetes cluster on scaleway


## Required vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| cluster_name | The name of cluster to create | `string` | `` | yes |
| scaleway_access_key | Access key of scaleway organization | `string` | `` | yes |
| scaleway_secret_key | Secret key of scaleway organization | `string` | `` | yes |
| scaleway_organization_id | Scaleway organization id | `string` | `` | yes |


## Other vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| cluster_version | The version of cluster to create | `string` | `1.20.1` | no |
| cluster_cni | The cni of cluster to create | `string` | `cilium` | no |
| cluster_pool_name | The pool name create with the cluster | `string` | `node_pool` | no |
| cluster_node_type | Node type in the cluster pool | `string` | `DEV1-M` | no |
| cluster_node_size | Node size in the cluster pool | `number` | `3` | no |
| scaleway_zone | Scaleway zone | `string` | `fr-par-1` | no |
| scaleway_region | Scaleway region | `string` | `fr-par` | no |


## Ouputs
| Name | Description |
|------|-------------|
| cluster_kubeconfig | The content of the cluster kubeconfig |


## Get the cluster kubeconfig after apply
```bash
terraform output --raw cluster_kubeconfig > kubeconfig.yml
```

## Usage 

```hcl-terraform
module "demo-k8s" {
  source                   = "git::https://gitlab.com/keltiotechnology/terraform/modules/scaleway/scaleway-k8s"

  cluster_name             = "My super cluster"
  scaleway_access_key      = "XXXXXXXXX"
  scaleway_secret_key      = "XXXXXXXXX"
  scaleway_organization_id = "XXXXXXXXX"
}
```
