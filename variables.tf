####################################################
#
# CLUSTER DATA
#
####################################################
variable "cluster_name" {
  type        = string
  description = "The name for the Kubernetes cluster"
}

variable "cluster_version" {
  type        = string
  default     = "1.20.1"
  description = "The version of the Kubernetes cluster"
}

variable "cluster_cni" {
  type        = string
  default     = "cilium"
  description = "The Container Network Interface (CNI) for the Kubernetes cluster. ~> Important: Updates to this field will recreate a new resource"
}

variable "cluster_pool_name" {
  type        = string
  default     = "node_pool"
  description = "The name for the pool. ~> Important: Updates to this field will recreate a new resource"
}

variable "cluster_node_type" {
  type        = string
  default     = "DEV1-M"
  description = "The commercial type of the pool instances. ~> Important: Updates to this field will recreate a new resource"
}

variable "cluster_node_size" {
  type        = number
  default     = 3
  description = "The minimum size of the pool, used by the autoscaling feature"
}

####################################################
#
# SCALEWAY AUTH
#
####################################################
variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_organization_id" {
  type = string
}

variable "scaleway_zone" {
  type        = string
  default     = "fr-par-1"
  description = "Specify your Availability Zones ( for Paris : fr-par-1, fr-par-2 | for Amsterdam : nl-ams-1)"
}

variable "scaleway_region" {
  type        = string
  default     = "fr-par"
  description = "Specify your region, Region is represented as a Geographical area such as France (Paris: fr-par) or the Netherlands (Amsterdam: nl-ams)"
}
