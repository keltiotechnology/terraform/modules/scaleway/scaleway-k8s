resource "scaleway_k8s_cluster_beta" "k8s_cluster" {
  name    = var.cluster_name
  version = var.cluster_version
  cni     = var.cluster_cni
}

resource "scaleway_k8s_pool_beta" "k8s_cluster_pool" {
  cluster_id = scaleway_k8s_cluster_beta.k8s_cluster.id
  name       = var.cluster_pool_name
  node_type  = var.cluster_node_type
  size       = var.cluster_node_size
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool_beta.k8s_cluster_pool] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster_beta.k8s_cluster.kubeconfig[0].host
    token                  = scaleway_k8s_cluster_beta.k8s_cluster.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster_beta.k8s_cluster.kubeconfig[0].cluster_ca_certificate
  }
}

provider "kubernetes" {
  load_config_file = "false"

  host  = null_resource.kubeconfig.triggers.host
  token = null_resource.kubeconfig.triggers.token
  cluster_ca_certificate = base64decode(
    null_resource.kubeconfig.triggers.cluster_ca_certificate
  )
}
