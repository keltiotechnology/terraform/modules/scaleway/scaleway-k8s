terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "1.17.2"
    }
  }
}

provider "scaleway" {
  access_key      = var.scaleway_access_key
  secret_key      = var.scaleway_secret_key
  organization_id = var.scaleway_organization_id
  zone            = var.scaleway_zone
  region          = var.scaleway_region
}
